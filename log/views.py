#!python
#log/views.py
import json
import os

from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.forms.models import model_to_dict
from django.views.decorators.csrf import csrf_exempt

from models import Bin, Activity
from helpers import path_finder

from utils.managers import bin_config_manager
from utils.commands import bin_command, delete_command, restore_command
from utils.helpers import clean_path

from django.conf import settings

@login_required(login_url="login/")
def home(request):
	return render(request,"home.html")


@login_required(login_url="login/")
def navigator(request, path='~'):
	if path == '':
		path = '~'
	[files, current, prev] = path_finder.parse_dir_path(path)
	for bin in Bin.objects.all():
		bin.check_availability()
		bin.save()
	return render(request, "navigator.html", {
		'files': files,
		'path': current,
		'prev': prev,
		'bins': Bin.objects.filter(is_available=True),
		'pathComponents': filter(None, current.split('/')),
		'activities': map(lambda x: {
			'type': x.type,
			'date': x.date,
			'description': x.description,
		}, Activity.objects.all().order_by('-date'))

	})

@login_required(login_url="login/")
def binmanager(request):
		for bin in Bin.objects.all():
			bin.check_availability()
			bin.save()
		return render(request, "binmanager.html", {
			'locations': map(
				lambda x : {
					'path': x.path,
					'id': x.id,
					'elem_count': str(len(json.loads(x.config)['history'])),
					'is_available': x.is_available,
				}, Bin.objects.all()),
		})

@csrf_exempt
# @login_required(login_url="login/")
def activities(request, id=None):
	return _abstract_get_post_view(request, id, Activity)




@csrf_exempt
# @login_required(login_url="login/")
def bins(request, id=None):
	model = Bin
	if request.method == 'GET':
		for bin in model.objects.all():
			bin.check_availability()
			bin.save()
		if id is None:
			queryset = model.objects.filter(is_available=True)
			queryset = json.dumps([model_to_dict(x) for x in queryset])
		else:
			queryset = json.dumps(model_to_dict(model.objects.get(id=id)))
		return HttpResponse(content=queryset)
	elif request.method == 'POST':
		try:

			obj = json.loads(request.body)
			obj = model(**obj)
			obj.config = json.dumps(bin_config_manager.DEFAULT_CONFIGS.BIN_CONFIG)
			if not bin_command.create_bin(obj.path,options={'config': {}}):
				obj.save()
				Bin.check_live_span()
				return HttpResponse(json.dumps({'status': 200}))
			else:
				raise Exception("Exception while creating bin occupired")
		except Exception as e:
			act = Activity()
			act.type = act.ERROR
			act.description = e.message
			act.save()
			return HttpResponse(json.dumps({'message': e.message, 'status': 403}))



def _abstract_get_post_view(request, id, model):
	if request.method == 'GET':
		if id is None:
			queryset = model.objects.all()
			queryset = json.dumps([model_to_dict(x) for x in queryset])
		else:
			queryset = json.dumps(model_to_dict(model.objects.get(id=id)))

		return HttpResponse(content=queryset)
	elif request.method == 'POST':
		obj = json.loads(request.body)
		obj = model(**obj)
		obj.save()
		return HttpResponse(
			json.dumps(model_to_dict(model.objects.get(id=obj.id)))
		)


def binmanagerdetls(request, id):

	bin = Bin.objects.get(id=id)
	[files, current, prev] = path_finder.parse_dir_path(bin.path)
	Bin.check_live_span()
	return render(request, "binmanagerdetls.html", {
		'files': files,
		'current': current,
		'id': id,
		'capacity_politics': bin.get_capacity_politics(),
		'current_capacity_politic': bin.get_current_politic(),
		'capacity_value_max': bin.capacity_value_max,
		'capacity_value': bin.get_capacity_value(),
		'options': bin.get_options(),
		'live_span': bin.live_span / 60.0 / 60 / 24,
	})

@csrf_exempt
def file_delete(request, id):
	if request.method == 'POST':
		bin = Bin.objects.get(id=id)
		paths = json.loads(request.body)['files']
		try:
			bin.validate_paths(paths)
			bin.capacity_before_delete(paths)
			delete_command.delete_async(paths, options={
				'path': bin.path,
				'bin': bin
			})
			bin.save()
			Bin.check_live_span()
			act = Activity()
			act.type = act.DELETE
			act.description = 'Delete %s to bin with path: %s' % (json.loads(request.body), bin.path)
			act.save()
			return HttpResponse(json.dumps({'status': 200}))
		except Exception as e:
			act = Activity()
			act.type = act.ERROR
			act.description = e.message
			act.save()
			return HttpResponse(json.dumps({'message': e.message, 'status': 403}))


@csrf_exempt
def bin_delete(request, id):
	try:
		if request.method == 'POST':
			bin = Bin.objects.get(id=id)
			path = bin.path
			bin.delete()
			clean_path.delete(bin.path)
			Bin.check_live_span()
			act = Activity()
			act.type = act.DELETE
			act.description = 'Delete bin with path %s' % (path)
			act.save()
			return HttpResponse(json.dumps({'status': 200}))
	except Exception as e:
		act = Activity()
		act.type = act.ERROR
		act.description = e.message
		act.save()
		return HttpResponse(json.dumps({'message': e.message, 'status': 403}))



@csrf_exempt
def bin_empty(request, id):
	try:
		if request.method == 'POST':
			bin = Bin.objects.get(id=id)
			bin_command.empty_bin(options={
				'path': bin.path,
				'bin': bin
			})
			bin.save()
			Bin.check_live_span()
			act = Activity()
			act.type = act.DELETE
			act.description = 'Delete everything from bin with path: %s' % (bin.path)
			act.save()
			return HttpResponse(json.dumps({'status': 200}))
	except Exception as e:
		act = Activity()
		act.type = act.ERROR
		act.description = e.message
		act.save()
		return HttpResponse(json.dumps({'message': e.message, 'status': 403}))



@csrf_exempt
def file_restore(request, id):
	if request.method == 'POST':
		try:
			files = json.loads(request.body)['files']
			bin = Bin.objects.get(id=id)
			bin.check_before_restore(files);
			restore_command.restore_async(files, options={
				'path': bin.path,
				'bin': bin
			})
			bin.save()
			act = Activity()
			act.type = act.RESTORE
			act.description = 'Restore %s from bin with path: %s' % (json.loads(request.body), bin.path)
			act.save()
			Bin.check_live_span()
			return HttpResponse(json.dumps({'status': 200}))
		except Exception as e:
			act = Activity()
			act.type = act.ERROR
			act.description = e.message
			act.save()
			return HttpResponse(json.dumps({'message': e.message, 'status': 403}))


@csrf_exempt
def bin_capacity_politic_change(request, id):
	if request.method == 'POST':
		options = json.loads(request.body)
		try:
			options = json.loads(request.body)
			bin = Bin.objects.get(id=id)
			bin.change_capacity_politic(new_politic=options['new_politic'])
			Bin.check_live_span()
			act = Activity()
			act.type = act.CHANGE_PROP
			act.description = 'Change capacity politic of %s to values:%s' % (bin.path, options['new_politic'])
			act.save()
			return HttpResponse(json.dumps({'status': 200}))
		except Exception as e:
			act = Activity()
			act.type = act.ERROR
			act.description = e.message
			act.save()
			return HttpResponse(json.dumps({'message': e.message, 'status': 403}))

@csrf_exempt
def bin_update_options(request, id):
	if request.method == 'POST':
		try:
			options = json.loads(request.body)['options']
			bin = Bin.objects.get(id=id)
			bin.change_options(options=options)
			Bin.check_live_span()
			act = Activity()
			act.type = act.CHANGE_PROP
			act.description = 'Change props of %s to values:%s' % (bin.path, options)
			act.save()

			return HttpResponse(json.dumps({'status': 200}))
		except Exception as e:
			act = Activity()
			act.type = act.ERROR
			act.description = e.message
			act.save()
			return HttpResponse(json.dumps({'message': e.message, 'status': 403}))

@csrf_exempt
def bin_live_span(request, id):
	if request.method == 'POST':
		options = json.loads(request.body)
		try:
			live_span = json.loads(request.body)['live-span']
			bin = Bin.objects.get(id=id)
			bin.change_live_span(live_span=live_span)
			act = Activity()
			act.type = act.CHANGE_PROP
			act.description = 'Change live span to %d of bin with by path %s' % (live_span, bin.path)
			act.save()

			Bin.check_live_span()
			return HttpResponse(json.dumps({'status': 200}))
		except Exception as e:
			act = Activity()
			act.type = act.ERROR
			act.description = e.message
			act.save()
			return HttpResponse(
				json.dumps({'message': e.message, 'status': 403}))



@csrf_exempt
# @login_required(login_url="login/")
def activities(request):
	if request.method == 'GET':
		queryset = Activity.objects.all()
		queryset = json.dumps([model_to_dict(x) for x in queryset])
		return HttpResponse(content=queryset)
	elif request.method == 'POST':
		try:
			obj = json.loads(request.body)
			obj = Activity(**obj)
			obj.save()
			return HttpResponse(json.dumps({'status': 200}))
		except Exception as e:
			act = Activity()
			act.type = act.ERROR
			act.description = e.message
			act.save()
			return HttpResponse(json.dumps({'message': e.message, 'status': 403}))
