import os
import copy
from datetime import datetime
from django.db import models
from utils.helpers import clean_path

from utils.managers import bin_config_manager
from setting.DEFAULT_CONFIGS import HISTORY_COPY_NAME_FORMAT, HISTORY_DATETIME_FORMAT

class Activity(models.Model):
    CHANGE_PROP = 'Change Prop'
    RESTORE = 'Restore'
    DELETE = 'Delete'
    EMPTY_BIN = 'Empty Bin'
    ERROR = 'Error'
    TYPE = (
        (RESTORE, 'Restore activity type'),
        (DELETE, 'Delete activity type'),
        (EMPTY_BIN, 'Empty activity type'),
        (CHANGE_PROP, 'Change prop type'),
        (ERROR, 'Error type'),

    )
    type = models.CharField(
        max_length=2,
        choices=TYPE,
    )
    date = models.DateTimeField(auto_now_add=True)
    description = models.CharField(max_length=5000)

class Bin(models.Model):
    BIN_NOT_VALID_MSG = 'Current bin is not valid...'
    POLITIC_NOT_SPECIFIED = 'Politic is not selected...'
    CAPACITY_OVERFLOW = 'Capacity overflows: can not delete %d. Clean %d from bin...'
    BIN_SELF_DELETING_ERROR_MSG = 'Active bin or its content can not be deleted...'
    BIN_DELETING_ERROR_MSG = 'Bin or its content can not be deleted by it\'s options...'
    WRONG_ARGUMENT = 'Something bad happens and server reseived wrong argumens...'
    DEFAULT_ERROR_MSG = 'Something went terrably wrong and i do not know what...'

    SIZE_CAPACITY_POLITIC = 'SCP'
    COUNT_CAPACITY_POLITIC = 'CCP'
    POLITIC_CHOICES = (
        (SIZE_CAPACITY_POLITIC, 'Size capacity politic'),
        (COUNT_CAPACITY_POLITIC, 'Count capacity politic'),
    )
    path = models.CharField(max_length=5000)
    is_available = models.BooleanField(default=False)
    config = models.CharField(max_length=5000)
    could_be_deleted = models.BooleanField(default=False)
    force_restore = models.BooleanField(default=False)
    live_span = models.IntegerField(default=6*24*60*60)

    capacity_politic = models.CharField(
        max_length=3,
        choices=POLITIC_CHOICES,
        default=COUNT_CAPACITY_POLITIC,
    )
    capacity_value = models.IntegerField(default=0)
    capacity_value_max = models.IntegerField(default=100)

    def check_availability(self):
        self.is_available = os.path.isdir(self.path)
        self.save()
        return self.is_available

    def get_capacity_value(self):
        if self.capacity_politic == self.COUNT_CAPACITY_POLITIC:
            return self.check_count()
        else:
            return self.check_size()

    def check_capacity(self):
        if self.check_availability():
            self.capacity_value = self.get_capacity_value()
            self.save()
            return self.capacity_value
        else:
            return None

    def check_count(self):
        if self.check_availability():
            count = clean_path.get_contains_count(self.path)
            self.capacity_value = count
            self.save()
            return count
        else:
            return None

    def check_size(self):
        if self.check_availability():
            size = clean_path.get_size(self.path)
            self.capacity_value = size
            self.save()
            return size
        else:
            return None

    def capacity_before_delete(self, paths):
        if not self.check_availability():
            raise Exception(Bin.BIN_NOT_VALID_MSG)
        else:
            current_capacity = self.get_capacity_value()
            if current_capacity is None:
                raise Exception(Bin.BIN_NOT_VALID_MSG)
            next_capacity = current_capacity + self.estimate_capacity(paths)
            if next_capacity > self.capacity_value_max:
                raise Exception(Bin.CAPACITY_OVERFLOW % (
                        self.estimate_capacity(paths),
                        next_capacity - self.capacity_value_max,
                    )
                )

    def validate_paths(self, paths):
        for path in paths:
            if path.startswith(self.path):
                raise Exception(self.BIN_SELF_DELETING_ERROR_MSG)

        for bin in Bin.objects.all():
            for path in paths:
                if path.startswith(bin.path) and not bin.could_be_deleted:
                    raise Exception(self.BIN_DELETING_ERROR_MSG)

    def estimate_capacity(self, paths):
        if self.capacity_politic == self.COUNT_CAPACITY_POLITIC:
            return len(paths)
        else:
            size = 0
            for path in paths:
                size += clean_path.get_size(path)
            return size

    def change_capacity_politic(self, new_politic):
        if 'name' in new_politic and new_politic['name'] in [i[0] for i in self.POLITIC_CHOICES]:
            self.capacity_politic = new_politic['name']
        if 'capacity_value_max' in new_politic and type(new_politic['capacity_value_max']) is int:
            self.capacity_value_max = new_politic['capacity_value_max']
        self.save()
        self.capacity_before_delete([])

    def get_capacity_politics(self):
        return [
            {
                'elem_name': 'elements',
                'politic': self.COUNT_CAPACITY_POLITIC,
            },
            {
                'elem_name': 'bytes',
                'politic': self.SIZE_CAPACITY_POLITIC,
            },
        ]

    def get_current_politic(self):
        return filter(lambda x: x['politic'] == self.capacity_politic, self.get_capacity_politics())[0]

    def change_options(self, options):
        if type(options) is not list:
            raise Exception(self.WRONG_ARGUMENT)
        else:
            for option in options:
                if 'name' not in option or 'value' not in option or type(option['name']) is not unicode or type(option['value']) is not bool:
                    raise Exception(self.WRONG_ARGUMENT)
                setattr(self, option['name'], option['value'])
            self.save()

    def get_options(self):
        return [
            {
                'true_name': 'could_be_deleted',
                'name': 'deletable',
                'value': getattr(self, 'could_be_deleted'),
            },
            {
                'true_name': 'force_restore',
                'name': 'force restore',
                'value': getattr(self, 'force_restore'),
            },
        ]

    def check_before_restore(self, files):
        if self.force_restore:
            return
        else:
            history = bin_config_manager.get_property('history', options={'bin': self})
            for history_item in history:
                src_name = history_item['src_name'];
                bin_name = history_item['bin_name'];
                src_dir = history_item['src_dir'];
                if os.path.join(self.path, bin_name) not in files:
                    continue
                count = 0
                if os.path.exists(os.path.join(src_dir, src_name)):
                    while True:
                        tmp_src_name = HISTORY_COPY_NAME_FORMAT.format(src_name, count)
                        print("hello")
                        if os.path.exists(os.path.join(src_dir, tmp_src_name)):
                            count += 1
                        else:
                            history_item['src_name'] = tmp_src_name
                            break

        bin_config_manager.set_property('history', history, options={'bin': self})
        self.save()

    def change_live_span(self, live_span):
        if type(live_span) is not int:
            raise Exception(self.WRONG_ARGUMENT)
        else:
            self.live_span = live_span
            self.save()
            return

    @staticmethod
    def check_live_span():
        try:
            bins = filter(lambda x: x.check_availability(), Bin.objects.all())
            for bin in bins:
                history = copy.deepcopy(bin_config_manager.get_property('history', options={'bin': bin}))
                for history_item in history:
                    deleting_time = datetime.strptime(history_item['date'], HISTORY_DATETIME_FORMAT)
                    now_time = datetime.now()
                    time_span = now_time - deleting_time
                    if time_span.seconds > bin.live_span:
                        bin_config_manager.history_del(history_item['bin_name'], options={'path': bin.path, 'bin': bin})
                bin.save()
        except Exception as e:
            raise Exception(Bin().DEFAULT_ERROR_MSG)

