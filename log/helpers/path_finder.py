import os
import glob


def parse_dir_path(_dir_path=None):
    try:
        dir_path = os.path.abspath(os.path.expanduser(_dir_path))
        if not os.path.isdir(dir_path):
            raise Exception('not dir exception')

        paths = map(
            lambda path: os.path.abspath(path),
            glob.glob(
                os.path.join(dir_path, '*')
            )
        )
        paths = map(
            lambda path: {
                'path': path,
                'isdir': os.path.isdir(path),
                'shortname': os.path.basename(path),
            },
            paths,
        )
        result_paths = filter(lambda path: path['isdir'], paths)
        result_paths += filter(lambda path: not path['isdir'], paths)
        return [result_paths, dir_path, os.path.dirname(dir_path)]
    except Exception as e:
        print e
        return e