#!python
# log/urls.py
from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^navigator/(?P<path>.*)$', views.navigator, name='navigator'),
    url(r'^bins/(?P<id>[0-9]+)$', views.bins, name='bin'),
    url(r'^bins$', views.bins, name='bins'),
    url(r'^activities/(?P<id>[0-9]+)$', views.activities, name='activity'),
    url(r'^activities$', views.activities, name='activities'),
    url(r'^binmanager$', views.binmanager, name='binmanager'),
    url(r'^binmanager/(?P<id>[0-9]+)$', views.binmanagerdetls, name='binmanager'),
    url(r'^filedelete/(?P<id>[0-9]+)$', views.file_delete, name='file_delete'),
    url(r'^bindelete/(?P<id>[0-9]+)$', views.bin_delete, name='bin_delete'),
    url(r'^binempty/(?P<id>[0-9]+)$', views.bin_empty, name='bin_empty'),
    url(r'^filerestore/(?P<id>[0-9]+)$', views.file_restore, name='file_restore'),
    url(r'^bincapacitypolitic/(?P<id>[0-9]+)$', views.bin_capacity_politic_change, name='bincapcitypolitic'),
    url(r'^binoption/(?P<id>[0-9]+)$', views.bin_update_options, name='bincoptions'),
    url(r'^livespan/(?P<id>[0-9]+)$', views.bin_live_span, name='binlivespan'),
]
