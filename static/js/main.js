var setErrorMessage = function _setErrorMessage(text) {
    var errorMessageFrame = document.getElementById('error-message-frame')
    if (errorMessageFrame) {
        var errorMessageTextField = document.getElementById('error-msg-text-field')
        if (errorMessageTextField) {
            errorMessageFrame.classList.remove('error-message-frame-active')
            errorMessageTextField.innerHTML = text;

            errorMessageFrame.classList.add('error-message-frame-active')
        }
    }
}
var setHandlers = function _setHandlers() {
    var selectionBtn = document.getElementById('select-all-btn');
    if(selectionBtn) {
        selectionBtn.onclick = function(e) {
            selectionBtn.classList.toggle('all-selected');
            document.querySelectorAll('#selection-checkbox').forEach(
                function(elem) {
                    if(selectionBtn.classList.contains('all-selected')) {
                        elem.checked = true;
                    }else {
                        elem.checked = false;
                    }

                }
            )
        };
    }


    var delBtns = document.querySelectorAll('.del-btn')
    if(delBtns) {
        delBtns.forEach(function(x) {
                x.onclick = function (e) {
                    var checkedItems = Array.from(document.querySelectorAll('#selection-checkbox')).filter(function (x) {
                        return x.checked
                    }).map(function (x) {
                        return x.attributes[0].value
                    });
                    var id = +x.id.slice(3)
                    $.post(
                        '/filedelete/' + id.toString(),
                        JSON.stringify({files: checkedItems}),
                        function(data, status) {
                            data = JSON.parse(data)

                            if (data.status == 403) {

                                setErrorMessage(data.message || 'some bad staff happened..')
                            }
                            else {
                                location.reload()
                            }

                        }
                    )

                }

        })

    }


    var newBinBtn = document.getElementById('new-bin-btn')
    if(newBinBtn) {
        newBinBtn.onclick = function (e) {
            var folder_name = document.getElementById('trash-name-value').value

            var location_path = document.getElementById('location-path').attributes[1].value

            var path = location_path + '/' + folder_name

            $.post(
                '/bins',
                JSON.stringify({path: path}),
                function(data, status) {
                            data = JSON.parse(data)

                            if (data.status == 403) {

                                setErrorMessage(data.message || 'some bad staff happened..')
                            }
                            else {
                                location.reload()
                            }

                        }
            )
        }
    }

    var trashNameValue = document.getElementById('trash-name-value')
    if(trashNameValue) {
        trashNameValue.onkeyup = function(e) {
            if(e.key == 'Enter') {
                newBinBtn.click()
            }
        }
    }

    var delBinBtn = document.getElementById('del-bin-btn')
    if(delBinBtn) {
        delBinBtn.onclick = function (e) {
            $.post(
                '/bindelete/' + +delBinBtn.attributes.data.value,
                JSON.stringify({}),
                function(data, status) {
                    data = JSON.parse(data)

                    if (data.status == 403) {

                        setErrorMessage(data.message || 'some bad staff happened..')
                    }
                    else {
                        location.assign('/binmanager')
                    }

                }
            )
        }
    }

    var emptyBinBtn = document.getElementById('empty-btn')
    if(emptyBinBtn) {
        emptyBinBtn.onclick = function (e) {
            $.post(
                '/binempty/' + emptyBinBtn.attributes.data.value,
                JSON.stringify({}),
                function(data, status) {
                    data = JSON.parse(data)

                    if (data.status == 403) {

                        setErrorMessage(data.message || 'some bad staff happened..')
                    }
                    else {
                        location.reload()
                    }

                }
            )
        }
    }

    var restoreBtn = document.getElementById('restore-btn')
    if(restoreBtn) {
        restoreBtn.onclick = function (e) {
            var checkedItems = Array.from(document.querySelectorAll('#selection-checkbox'))
                .filter(function (x) {return x.checked}).map(function (x) {return x.attributes[0].value});
            $.post(
                '/filerestore/' + restoreBtn.attributes.data.value,
                JSON.stringify({
                    files: checkedItems
                }),
                function(data, status) {
                    data = JSON.parse(data)

                    if (data.status == 403) {

                        setErrorMessage(data.message || 'some bad staff happened..')
                    }
                    else {
                        location.reload()
                    }

                }
            )
        }
    }

    var contentList = document.getElementById('content-list')
    if(contentList) {
        var titles = document.querySelectorAll('.doc-header')
        contentList.innerHTML = Array.from(titles).map(function(x) {
            return '<li><p><a href="#' + x.id + '">' + x.innerHTML + '</a></p></li>'
        }).join('')

    }

    var pathComponents = document.querySelectorAll('#path_component')
    if(pathComponents.length){
        pathComponents.forEach(function(x) {
            x.onclick = function(e) {
                toUse = Array.from(pathComponents).filter(function(i) {

                    return +i.attributes[1].value <= +x.attributes[1].value
                })
                toUse = '/' + toUse.map(function(x){return x.innerText}).join('')
                location.assign('/navigator/' + toUse)


            }
            x.onmouseover = function(e) {
                toChange = Array.from(pathComponents).filter(function(i) {
                    return +i.attributes[1].value <= +x.attributes[1].value
                })
                toChange.forEach(function(x) {
                    x.classList.add('boldness')
                })
            }
            x.onmouseout = function(e) {
                pathComponents.forEach(function(x) {
                    x.classList.remove('boldness')
                })
            }
        })
    }

    var notAvailableBtnsDelete= document.querySelectorAll('#del-bin-btn-not-available')
    if(notAvailableBtnsDelete) {
        Array.from(notAvailableBtnsDelete).forEach(
            function(btn) {
                btn.onclick = function(e) {
                    $.post(
                        '/bindelete/' + +btn.attributes.data.value,
                        JSON.stringify({}),
                        function(data, status) {
                            data = JSON.parse(data)

                            if (data.status == 403) {

                                setErrorMessage(data.message || 'some bad staff happened..')
                            }
                            else {
                                location.reload()
                            }

                        }
                    )
                }
                btn.onmouseenter = function(e) {
                    btn.innerHTML = 'delete'
                }
                btn.onmouseleave = function(e) {
                    btn.innerHTML = 'not exists'
                }
            }
        )
    }

    var errorPopupCloseBtn = document.querySelector('#error-frame-close-btn')

    if (errorPopupCloseBtn) {
        var errorFrame = document.querySelector('.error-message-frame')

        errorPopupCloseBtn.onclick = function() {

            errorFrame.classList.remove('error-message-frame-active')
        }
    }

    var autoAdjustInputs = document.querySelectorAll('#auto-adjust-input')
    if (autoAdjustInputs) {
        Array.from(autoAdjustInputs).forEach(
            function(input) {
                function onUpdate() {
                    if (input.attributes['bin-old-max-value'].value === input.value) {
                        return
                    }
                    $.post(
                        '/bincapacitypolitic/' + +input.attributes.bin_id.value,
                        JSON.stringify({'new_politic': {
                            capacity_value_max: +input.value
                        }}),
                        function(data, status) {
                            data = JSON.parse(data)
                            if (data.status == 403) {
                                setErrorMessage(data.message || 'some bad staff happened..')
                            }
                            else {
                                location.reload()
                            }

                        }
                    )
                }
                input.onblur = onUpdate
                input.onkeyup = function(e) {
                    if (e.key == 'Enter')
                        input.blur()
                        // onUpdate()
                }
                function resizeInput() {
                   $(this).attr('size', $(this).val().length || 1);
                }
                $(input)
                    .keyup(resizeInput)
                    .each(resizeInput);
            }
        )

    }

    var autoAdjustInputLiveSpan = document.querySelectorAll('#auto-adjust-input-live-span')
    if (autoAdjustInputLiveSpan) {
        Array.from(autoAdjustInputLiveSpan).forEach(
            function(input) {
                function onUpdate() {
                    if (input.attributes['bin-old-max-value'].value === input.value) {
                        return
                    }
                    $.post(
                        '/livespan/' + +input.attributes.bin_id.value,
                        JSON.stringify({'live-span': Math.round((+input.value) * 60 * 60 * 24)}),
                        function(data, status) {
                            data = JSON.parse(data)
                            if (data.status == 403) {
                                setErrorMessage(data.message || 'some bad staff happened..')
                            }
                            else {
                                location.reload()
                            }

                        }
                    )
                }
                input.onblur = onUpdate
                input.onkeyup = function(e) {
                    if (e.key == 'Enter')
                        input.blur()
                        // onUpdate()
                }
                function resizeInput() {
                   $(this).attr('size', $(this).val().length || 1);
                }
                $(input)
                    .keyup(resizeInput)
                    .each(resizeInput);
            }
        )

    }


    var politicActivateBtns = document.querySelectorAll('#politic-activate-btn')
    if (politicActivateBtns) {
        Array.from(politicActivateBtns).forEach(
            function(btn) {
                function onUpdate() {
                    $.post(
                        '/bincapacitypolitic/' + +btn.attributes.bin_id.value,
                        JSON.stringify({'new_politic': {
                            name: btn.attributes['politic-name'].value
                        }}),
                        function(data, status) {
                            data = JSON.parse(data)
                            if (data.status == 403) {
                                setErrorMessage(data.message || 'some bad staff happened..')
                            }
                            else {
                                location.reload()
                            }

                        }
                    )
                }
                btn.onclick = onUpdate

            }
        )
    }

    var optionsChanger = document.getElementById('bin-option-changer')
    console.log(optionsChanger)
    if (optionsChanger) {
        optionsChanger.onclick = function(e) {
            var options = document.querySelectorAll('#option-checkbox')
            options = Array.from(options).map(
                function(input) {
                    console.log(input)
                    return {
                        'name': input.attributes['true-name'].value,
                        'value': input.checked,
                    }
                }
            )
            $.post(
                '/binoption/' + +optionsChanger.attributes['bin-id'].value,
                JSON.stringify({'options': options}),
                function(data, status) {
                    data = JSON.parse(data)
                    if (data.status == 403) {
                        setErrorMessage(data.message || 'some bad staff happened..')
                    }
                    else {
                        location.reload()
                    }

                }
            )
        }
    }


}

document.onready = function() {
    setHandlers();
}

